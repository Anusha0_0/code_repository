Algorithm:
step 1: Start
step 2 : Declare variable n
step 3: Read variable n
step 4: Write multipliction table of n using looping statements
step 5: End
Code:
#include<stdio.h>
int main()
{
	int i,n;
	printf("Enter an integer\n");
	scanf("%d", &n);
	for(i=1;i<=10; i++)
	{
		printf("%d*%d = %d\n",n,i,n*i);
	}
		return 0;
}
	